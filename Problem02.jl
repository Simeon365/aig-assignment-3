### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ e4859920-d80c-11eb-368f-cdae57da7dd8
using Pkg

# ╔═╡ 31f30072-d859-11eb-27fa-97dbdf1032c2
Pkg.activate("Project.toml")

# ╔═╡ e457e26e-d909-11eb-3569-0b1cbeaca6d3
# creates an empty board that will be used for gameplay
board = [[" ", " ", " "],
	     [" ", " ", " "],
	     [" ", " ", " "]]

# ╔═╡ 73174902-d9a1-11eb-22f6-1faa05c1bd72
 md"# Player Definitions "

# ╔═╡ f94eb4b0-d909-11eb-3429-73792fd9c5d8
player1 = "X"

# ╔═╡ f94f50f0-d909-11eb-0984-513f6da90d6e
player2 = "O"

# ╔═╡ f961ee90-d909-11eb-089a-dfaf0f840fd6
player1_win = -1

# ╔═╡ f9721b30-d909-11eb-31b2-69f5ed834a75
player2_win = 1

# ╔═╡ f9810f50-d909-11eb-0ddc-09e661bd8a68
tie = 0

# ╔═╡ f9909fb0-d909-11eb-31c7-15b1bd1e5648
function print_board(board)
	    println("    1   2   3")
	    println(" 1  ", join(board[1], " | "))
	    println("   ———┼———┼———")
	    println(" 2  ", join(board[2], " | "))
	    println("   ———┼———┼———")
	    println(" 3  ", join(board[3], " | "))
	end

# ╔═╡ f9a0a540-d909-11eb-18fd-f1a494eaa03b
function check_available_cells(board)
	    free_cells = []
	    for i in 1:3, j in 1:3
	        if board[i][j] == " "
	            push!(free_cells, (i, j))
	        end
	    end
	    return free_cells
	end

# ╔═╡ f9b0f8ee-d909-11eb-0a2a-6b2ff473b843
function evaluate_win(board)
	
	    # Rows
	    for i in 1:3
	        if (board[i][1] == board[i][2] == board[i][3]) && (board[i][1] in [player1, player2])
	            if (board[i][1] == player1)
	                return player1_win
	            end
	            return player2_win
	        end
	    end
	
	    # Columns
	    for i in 1:3
	        if (board[1][i] == board[2][i] == board[3][i]) && (board[1][i] in [player1, player2])
	            if (board[1][i] == player1)
	                return player1_win
	            end
	            return player2_win
	        end
	    end
	
	    # Principal diagonal
	    if (board[1][1] == board[2][2] == board[3][3]) && (board[1][1] in [player1, player2])
	        if (board[1][1] == player1)
	            return player1_win
	        end
	        return player2_win
	    end
	
	    # Other diagonal
	    if (board[1][3] == board[2][2] == board[3][1]) && (board[1][3] in [player1, player2])
	        if (board[1][3] == player1)
	            return player1_win
	        end
	        return player2_win
	    end
	
	    # Tie
	    if length(check_available_cells(board)) == 0
	        return tie
	    end
	
	    # No one wins
	    return nothing
	end

# ╔═╡ f9c173b0-d909-11eb-1372-6bae059020a5
function make_move!(board, player, (x, y))
	    board[x][y] = player
	end

# ╔═╡ fa216e02-d909-11eb-019c-eb7c355834df
function minimax(board, depth, is_maximising)
	    result = evaluate_win(board)
	
	    if result !== nothing
	        return result
	    elseif is_maximising
	
	        best_score = -Inf
	
	        for i in 1:3, j in 1:3
	            if board[i][j] == " "
	
	                board[i][j] = player2
	
	                score = minimax(board, depth + 1, false)
	                
	                board[i][j] = " "
	
	                best_score = max(score, best_score)
	            end
	        end
	
	        return best_score        
	    else        
	        best_score = Inf
	
	        for i in 1:3, j in 1:3
	            if board[i][j] == " "
	
	                board[i][j] = player1
	
	                score = minimax(board, depth + 1, true)
	                
	                board[i][j] = " "
	
	                best_score = min(score, best_score)
	            end
	        end
	
	        return best_score   
	    end
	
	end

# ╔═╡ f9d08ede-d909-11eb-3682-0d0d4114f021
function worst_move(board)
	    worst_score = Inf
	
	    worst_position = (0, 0)
	
	    for i in 1:3, j in 1:3
	        if board[i][j] == " "
	
	            board[i][j] = player2
	            
	            score = minimax(board, 0, true)
	
	            
	            board[i][j] = " "
	
	            if score < worst_score
	                worst_score = score
	                worst_position = (i, j)
	            end
	        end
	    end
	    return worst_position
	end

# ╔═╡ fa4570c0-d909-11eb-31ab-01a9b55618cf
function best_move(board)
	    best_score = -Inf
	
	    best_position = (0, 0)
	
	    for i in 1:3, j in 1:3
	        if board[i][j] == " "
	
	            board[i][j] = player2
	            
	            score = minimax(board, 0, false)
	
	            
	            board[i][j] = " "
	
	            if score > best_score
	                best_score = score
	                best_position = (i, j)
	            end
	        end
	    end
	    return best_position
	end

# ╔═╡ f9fad330-d909-11eb-2f3b-f37b2cf7970a
function computer_move!(board, difficulty)
	    if difficulty == "amateur"
	        make_move!(board, player2, worst_move(board))
	    elseif difficulty == "easy"
	        position = rand(check_available_cells(board))
	        make_move!(board, player2, position)
	    elseif difficulty == "medium"
	        if rand() < 0.3
	            make_move!(board, player2, best_move(board))
	        else            
	            position = rand(check_available_cells(board))
	            make_move!(board, player2, position)
	        end
	    elseif difficulty == "hard"
	        if rand() < 0.7
	            make_move!(board, player2, best_move(board))
	        else            
	            position = rand(check_available_cells(board))
	            make_move!(board, player2, position)
	        end
	    elseif difficulty == "impossible"
	        make_move!(board, player2, best_move(board))
	    end
	    
	end

# ╔═╡ fa599500-d909-11eb-1d86-51f9c966fbe6
function move_input(board, prompt, parameters)
	
	    print(prompt)
	    txt = split(readline())
	
	    if txt == ["exit"]
	        exit()
	    end
	
	    if length(txt) == parameters && txt[1] in string.(collect(1:3)) && txt[2] in string.(collect(1:3)) && (board[parse(Int, txt[1])][parse(Int, txt[2])] == " ")
	        return txt
	    else
	        println("Please enter a valid input")
	        move_input(board, prompt, parameters)
	    end
	end

# ╔═╡ fa7c8650-d909-11eb-2b18-899103d8d38d
function general_input(prompt, same_line, choices, parameters)
	    same_line ? print(prompt) : println(prompt)
	
	    txt = split(lowercase(readline()))
	
	    if txt == ["Exit"]
	        exit()
	    end
	
	    if length(txt) == parameters && txt[1] in choices
	        return txt
	    else
	        println("Please enter a valid input")
	        general_input(prompt, same_line, choices, parameters)
	    end
	end

# ╔═╡ faaf7d32-d909-11eb-25b8-f1c85747a747
function singleplayer(board, (player1, player2), (player1_win, player2_win, tie))
	    different_levels = """
	    Choose a difficulty level:
	    - `amateur` - Computer always makes worst move
	    - `easy` - Computer plays randomly
	    - `medium` - Computer makes a few mistakes
	    - `hard` - Computer makes very few mistakes
	    - `impossible` - Computer always makes the best move. Impossible to win.
	    """
	
	    println(different_levels)
	    difficulty = general_input("Difficulty: ", true, ["amateur", "easy", "medium", "hard", "impossible"], 1)[1]
	
	    println("You are X, Computer is O. You go first.")
	    print_board(board)
	
	    while true
	
	        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)
	
	        make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))
	
	        if evaluate_win(board) == player1_win
	            println("You win!")
	            break
	        elseif evaluate_win(board) == player2_win
	            println("Computer wins!")
	            break
	        elseif evaluate_win(board) == tie
	            println("Tie!")
	            break
	        end
	
	        computer_move!(board, difficulty)
	        print_board(board)
	
	        if evaluate_win(board) == player1_win
	            println("You win!")
	            break
	        elseif evaluate_win(board) == player2_win
	            println("Computer wins!")
	            break
	        elseif evaluate_win(board) == tie
	            println("Tie!")
	            break
	        end
	    end
	end

# ╔═╡ fad4df80-d909-11eb-3505-f5fb690730e4
function multiplayer(board, (player1, player2), (player1_win, player2_win, tie))
	    println("Player 1 is X, Player 2 is O. Player 1 goes first.")
	    print_board(board)
	
	    while true
	
	        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)
	
	        make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))
	        print_board(board)
	
	        if evaluate_win(board) == player1_win
	            println("Player 1 Wins!")
	            break
	        elseif evaluate_win(board) == player2_win
	            println("Player 2 Wins!")
	            break
	        elseif evaluate_win(board) == tie
	            println("Tie!")
	            break
	        end
	
	        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)
	
	        make_move!(board, player2, (parse(Int, txt[1]), parse(Int, txt[2])))
	
	        print_board(board)
	
	
	        if evaluate_win(board) == player1_win
	            println("Player 1 Wins!")
	            break
	        elseif evaluate_win(board) == player2_win
	            println("Player 2 Wins!")
	            break
	        elseif evaluate_win(board) == tie
	            println("Tie!")
	            break
	        end
	    end
	end

# ╔═╡ faf93060-d909-11eb-0947-b5616adbfd34
function play_again()
	    replay = general_input("Would you like to play again? (y/n): ", true, ["y", "n"], 1)[1]
	    if replay == "y"
	        run(`julia $PROGRAM_FILE`)
	        exit()
	    else
	        exit()
	    end
	end

# ╔═╡ fb282fa0-d909-11eb-2bb7-7b26652291dd
mode = general_input("Welcome to TicTacToe! Would you like to play singleplayer (1) or multiplayer (2)?", false, ["1", "2"], 1)[1]

# ╔═╡ fb45a2b0-d909-11eb-1baa-e97d70b7813b
if mode == "1"
	    singleplayer(board, (player1, player2), (player1_win, player2_win, tie))
	else
	    multiplayer(board, (player1, player2), (player1_win, player2_win, tie))
	end

# ╔═╡ fb5fba60-d909-11eb-1f8e-a15a66a19fa6
play_again()

# ╔═╡ Cell order:
# ╠═e4859920-d80c-11eb-368f-cdae57da7dd8
# ╠═31f30072-d859-11eb-27fa-97dbdf1032c2
# ╠═e457e26e-d909-11eb-3569-0b1cbeaca6d3
# ╟─73174902-d9a1-11eb-22f6-1faa05c1bd72
# ╠═f94eb4b0-d909-11eb-3429-73792fd9c5d8
# ╠═f94f50f0-d909-11eb-0984-513f6da90d6e
# ╠═f961ee90-d909-11eb-089a-dfaf0f840fd6
# ╠═f9721b30-d909-11eb-31b2-69f5ed834a75
# ╠═f9810f50-d909-11eb-0ddc-09e661bd8a68
# ╠═f9909fb0-d909-11eb-31c7-15b1bd1e5648
# ╠═f9a0a540-d909-11eb-18fd-f1a494eaa03b
# ╠═f9b0f8ee-d909-11eb-0a2a-6b2ff473b843
# ╠═f9c173b0-d909-11eb-1372-6bae059020a5
# ╠═f9d08ede-d909-11eb-3682-0d0d4114f021
# ╠═f9fad330-d909-11eb-2f3b-f37b2cf7970a
# ╠═fa216e02-d909-11eb-019c-eb7c355834df
# ╠═fa4570c0-d909-11eb-31ab-01a9b55618cf
# ╠═fa599500-d909-11eb-1d86-51f9c966fbe6
# ╠═fa7c8650-d909-11eb-2b18-899103d8d38d
# ╠═faaf7d32-d909-11eb-25b8-f1c85747a747
# ╠═fad4df80-d909-11eb-3505-f5fb690730e4
# ╠═faf93060-d909-11eb-0947-b5616adbfd34
# ╠═fb282fa0-d909-11eb-2bb7-7b26652291dd
# ╠═fb45a2b0-d909-11eb-1baa-e97d70b7813b
# ╠═fb5fba60-d909-11eb-1f8e-a15a66a19fa6
