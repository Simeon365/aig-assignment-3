### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 98cc6dce-d8fa-11eb-288e-81a1c623df54
using Pkg

# ╔═╡ c2e621e0-dd0c-11eb-104a-8d1ff2351447
using POMDPs, QuickPOMDPs, POMDPModelTools, POMDPSimulators, QMDP

# ╔═╡ 97fa9ede-d8fa-11eb-0c13-e1300c52c0df
Pkg.activate("Project.toml")

# ╔═╡ a9a17950-dd0c-11eb-2294-f3f00c2ea976
m = QuickPOMDP(
    states = ["left", "right"],
    actions = ["left", "right", "listen"],
    observations = ["left", "right"],
    initialstate = Uniform(["left", "right"]),
    discount = 0.95,

    transition = function (s, a)
        if a == "listen"
            return Deterministic(s) 
        else 
            return Uniform(["left", "right"])
        end
    end,

    observation = function (s, a, sp)
        if a == "listen"
            if sp == "left"
                return SparseCat(["left", "right"], [0.85, 0.15]) 
            else
                return SparseCat(["right", "left"], [0.85, 0.15])
            end
        else
            return Uniform(["left", "right"])
        end
    end,

    reward = function (s, a)
        if a == "listen"
            return -1.0
        elseif s == a 
            return -100.0
        else
            return 10.0
        end
    end
)

# ╔═╡ bcd20a80-dd0c-11eb-2a78-5778a593f9ee
solver = QMDPSolver()

# ╔═╡ bcd31bf0-dd0c-11eb-36ce-c9cb427bb5e1
policy = solve(solver, m)

# ╔═╡ bcf68270-dd0c-11eb-0d37-47c7564dd193
println("Undiscounted reward was $rsum.")

# ╔═╡ bce03b50-dd0c-11eb-3a38-6fca6fe71708
rsum = 0.0

# ╔═╡ bcec2230-dd0c-11eb-1842-3fcbea865c97
for (s,b,a,o,r) in stepthrough(m, policy, "s,b,a,o,r", max_steps=10)
    println("s: $s, b: $([pdf(b,s) for s in states(m)]), a: $a, o: $o")
    global rsum += r
end

# ╔═╡ Cell order:
# ╠═98cc6dce-d8fa-11eb-288e-81a1c623df54
# ╠═97fa9ede-d8fa-11eb-0c13-e1300c52c0df
# ╠═c2e621e0-dd0c-11eb-104a-8d1ff2351447
# ╠═a9a17950-dd0c-11eb-2294-f3f00c2ea976
# ╠═bcd20a80-dd0c-11eb-2a78-5778a593f9ee
# ╠═bcd31bf0-dd0c-11eb-36ce-c9cb427bb5e1
# ╠═bce03b50-dd0c-11eb-3a38-6fca6fe71708
# ╠═bcec2230-dd0c-11eb-1842-3fcbea865c97
# ╠═bcf68270-dd0c-11eb-0d37-47c7564dd193
